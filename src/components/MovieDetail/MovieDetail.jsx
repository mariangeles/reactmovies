// const { default: MovieCharacter } = require("../MovieCharacter/MovieCharacter");
import MovieCharacter from '../MovieCharacter';
import './styles.css';

function MoviesDetail ({ movie }) {
    return (
        <div className='movie-detail-container'>
            <div className="details-container">
                <div className="title-container">
                    <h1>{movie.title}</h1>
                    <img className='movie-picture' src={movie.img} alt={movie.title} />
                </div>
                <div>
                    <h3>Sinopsis</h3>
                    <p>{movie.description}</p>
                </div>
            </div>

            <h2>Lista de personajes</h2>
            <div className="characters-container">
                {
                    movie.characters.length > 0 ?
                        movie.characters.map(character => <MovieCharacter character={ character } />) 
                        :<p>No hay nada </p>
                }
            </div>
            {/* CHARACTER */}
            {/* {movie.characters.map(character => <h3>{character.name}</h3>)} */}
            
            {/* <MovieCharacter character={movie.characters.map(character => character)}/> */}
        </div>
    )
}

export default MoviesDetail;