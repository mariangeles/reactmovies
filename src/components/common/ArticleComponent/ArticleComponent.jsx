function ArticleComponent ({ children }) {
    return(
        <article>
            { children }
        </article>
    )
}
export default ArticleComponent;