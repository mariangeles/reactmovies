import './styles.css';
//renderiza todo lo que esta dentro del componente
//recibe props = children
function AppContainer(props) {
    return (
        <div className = "App" >
            <header className = "App-header" >
                {props.children}
            </header>
        </div>
    )
}

export default AppContainer;