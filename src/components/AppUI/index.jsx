
import AppContainer from '../common/AppContainer/'
import ImageComponent from '../common/ImageComponent';
import logo from '../../logo.svg';
import ParagraphComponent from '../common/ParagraphComponent';
import LinkComponent from '../common/LinkComponent/LinkComponent';
import ButtonComponent from '../common/ButtonComponent/ButtonComponent';



function AppUI(props) {
    return (
        <AppContainer>
                <ImageComponent img={logo} />
                {/* <p>
                    Hola Mari
                </p>  */}
                <ParagraphComponent text='Hola Mundo desde react'/>

                <LinkComponent url={"https://github.com/mari8793"} title={"Checa mi github"}/>
                {/* <ButtonComponent handleClick={hola()} text={'Pikale aquí'}/> */}
                {/* <ButtonComponent handleClick={this.increment} text={'Pikale aquí'}/> */}
                {/* <p>Contador: {props.counter}</p> */}
                {/* <ParagraphComponent text={`Contador: ${props.counter}`}/> */}

        </AppContainer>
    )
}
export default AppUI