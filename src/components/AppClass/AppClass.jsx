import { Component } from 'react'
import AppContainer from '../common/AppContainer/'
import ImageComponent from '../common/ImageComponent';
import logo from '../../logo.svg';
import ParagraphComponent from '../common/ParagraphComponent';
import LinkComponent from '../common/LinkComponent';
import ButtonComponent from '../common/ButtonComponent';
import TitleComponent from '../common/TitleComponent';
import ArticleComponent from '../common/ArticleComponent'

//  import AppClass from '.'
// import AppUI from '../AppUI'

//Componente de clase
class AppClass extends Component{    
    constructor() {
        super()

        //EXISTE EN REACT.COMPONENT
        this.state = {
            counter : 0,
            // chelas: false,
            // comida: true
        }
    }
    increment = () => {
        //setState puede tener un callback (es el console.log) seState es asincrono 
        // this.setState({
        //     counter: this.state.counter + 1
        // }, () => console.log(this.state.counter))
        //el counter no se ha actualizado entonces se ejecuta el console (asincrono), por eso se desfasa el ui y la consola
        // console.log(this.state.counter)

        this.setState((prevState) => {
            // console.log(prevState.counter)
            return { counter: prevState.counter + 1 }
        }, () => console.log(this.state.counter))
    }
    render(){
        const { counter } = this.state;
        //pasar funciones o valores de un componente a otro (padre a hijo) PROPS
        //return <AppUI counter={this.state.counter} handleClick={this.increment}/>
        return (
            <>
                <AppContainer>
                        <ImageComponent img={logo} />
                        <ParagraphComponent text='Hola Mundo desde react'/>
                        <LinkComponent url={"https://github.com/mari8793"} title={"Checa mi github"}/>
                        <ButtonComponent handleClick={() => this.increment()} text={'Pikale aquí'}/>
                        {/* <p>Contador: {props.counter}</p> */}
                        {/* <ParagraphComponent text={`Contador: ${counter}`}/> */}
                        <TitleComponent text={ counter }/>

                </AppContainer>
                <ArticleComponent>
                    <TitleComponent text={'Hola estoy en un article'}/>
                </ArticleComponent>
            </>

        )
    }
}

export default AppClass;