import { useState } from 'react'
import AppContainer from '../common/AppContainer';
import ButtonComponent from '../common/ButtonComponent';
import ImageComponent from '../common/ImageComponent';
import LinkComponent from '../common/LinkComponent';
import ParagraphComponent from '../common/ParagraphComponent';
// import AppUI from '../AppUI';



//Componente de clase 
function AppFunction() {
    const[counter, setCounter] = useState(0);
    function incrementCounter () {
        //counter +=1 Es solo de lectura, aquí no hacemos nada de eso porque lo almacenamos en un estado, no en una variable
        setCounter(counter + 1)
    }
    // return <AppUI counter={counter} handleClick={incrementCounter}/>
    return (
        <AppContainer>
            <ImageComponent img={'https://assets.stickpng.com/thumbs/5aafaf2f7603fc558cffc099.png'} />
            <ParagraphComponent text={'Mira, este es mi componente de función con un logo de Kirby'} classN={'rojo'}/>
            <LinkComponent url={'https://es.wikipedia.org/wiki/Kirby_(personaje)'} title={'Mira el Kirby'}/>
            <ButtonComponent handleClick={incrementCounter} text={'Púchale'}/>

        </AppContainer>
    )
}
export default AppFunction;